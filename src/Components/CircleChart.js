import React, { PureComponent } from 'react';
import {
  ResponsiveContainer, PieChart, Pie, Legend, Cell,
} from 'recharts';

const data = [
  { name: 'Bathroom', value: 400 }, { name: 'LivingRoom', value: 300 },
  { name: 'Entrance', value: 300 }, { name: 'Bedroom', value: 200 },
];

const COLORS = ['#3C9BEC', '#330066', '#FFBB28', '#FF8042'];

export default class Example extends PureComponent {
  static jsfiddleUrl = '//jsfiddle.net/alidingling/6okmehja/';

  render() {
    return (
      <div style={{ width: '100%', height: 300 }}>
        <ResponsiveContainer>
          <PieChart>
            <Pie dataKey="value" data={data} fill="#8884d8" label>
            {
              data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
            }
            </Pie>
          </PieChart>
        </ResponsiveContainer>
      </div>
    );
  }
}
