import React, {Component} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {Link} from "react-router-dom";

class App extends Component {
  render(){

    return (
      <Navbar bg="dark" variant="dark">
      <Link to="/">
              <Navbar.Brand href="#">Accueil</Navbar.Brand>
      </Link>
        <Nav className="mr-auto">
          <Nav.Link href="#">Home</Nav.Link>
          <Nav.Link href="#">Features</Nav.Link>
          <Nav.Link href="#">Pricing</Nav.Link>
        </Nav>
        <Form inline>
        <Link to="/Form">
        <Button variant="outline-info">Formulaire de données</Button>
        </Link>
        </Form>
      </Navbar>
    );
  }
}

export default App;
