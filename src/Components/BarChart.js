import React, { PureComponent } from 'react';
import {
  ResponsiveContainer, ComposedChart, Line, Area, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
  {
    name: 'Janv', Degre: 590, Air: 800, Humidity: 1400,
  },
  {
    name: 'Fev', Degre: 868, Air: 967, Humidity: 1506,
  },
  {
    name: 'Mars', Degre: 1397, Air: 1098, Humidity: 989,
  },
  {
    name: 'Avril', Degre: 1480, Air: 1200, Humidity: 1228,
  },
  {
    name: 'Mai', Degre: 1520, Air: 1108, Humidity: 1100,
  },
  {
    name: 'Juin', Degre: 1400, Air: 680, Humidity: 1700,
  },
];

export default class Example extends PureComponent {
  static jsfiddleUrl = '//jsfiddle.net/alidingling/9wnuL90w/';

  render() {
    return (
      <div style={{ width: '100%', height: 300 }}>
        <ResponsiveContainer>
          <ComposedChart
            width={500}
            height={400}
            data={data}
            margin={{
              top: 20, right: 20, bottom: 20, left: 20,
            }}
          >
            <CartesianGrid stroke="#f5f5f5" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="Degre" stroke="#ff7300" />
            <Line type="monotone" dataKey="Air" stroke="#3C9BEC" />
            <Line type="monotone" dataKey="Humidity" stroke="#330066" />
          </ComposedChart>
        </ResponsiveContainer>
      </div>
    );
  }
}
