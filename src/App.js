import React from 'react';
import './App.css';
import Dashboard from './Components/Dashboard.js';
import NavBar from './Components/NavBar.js';
import Form from './Components/Form.js';
import Footer from './Components/Footer.js';
import { BrowserRouter, Route } from "react-router-dom";

function App() {
  return (
    <div>
    <BrowserRouter>
        <NavBar></NavBar>
        <Route path="/" exact component={Dashboard}></Route>
        <Route path="/Form" exact component={Form}></Route>
        <Footer></Footer>
    </BrowserRouter>
    </div>
  );
}

export default App;
